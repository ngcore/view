# @ngcore/view
> NG Core angular/typescript view library


Simple UI/view classes library for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/view/




