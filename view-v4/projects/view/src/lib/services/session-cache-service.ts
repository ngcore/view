// import { Injectable } from '@angular/core';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, DateIdUtil } from '@ngcore/core';
import { BaseModel } from '@ngcore/base';


// @//Injectable()
export class SessionCacheService {

  private get todayId() {
    let today = DateIdUtil.getTodayId();
    return today;
  }

  // To be used while the app is running.
  private _cache: {[key: string]: any } = {};
  // private _cache: {[key: string]: string | number | boolean | BaseModel } = {};


  // constructor(
  //   private alertCtrl: AlertController,
  //   private toastCtrl: ToastController,
  //   private events: Events,
  // ) {
  //   if(isDL()) dl.log('Hello SessionCacheService Provider');
  // }

  private static _Instance: (SessionCacheService | null) = null;
  private constructor() { }
  public static getInstance(): SessionCacheService {
    return this._Instance || (this._Instance = new SessionCacheService());
  }


  hasKey(key: string): boolean {
    // return (key in this._cache);
    // return (this._cache.hasOwnProperty(key));
    return (this._cache[key] !== undefined);
  }


  put(key: string, value: any = null) {
    // tbd: just overwrite, if entry with given key already exists?
    this._cache[key] = value;
  }


  remove(key: string) {
    delete this._cache[key];
  }

  clear() {
    this._cache = {};
  }


  fetchNumber(key: string): (number | undefined) {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as number;
    } else {
      return undefined;
    }
  }
  getNumber(key: string, defaultVal: number): number {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as number;
    } else {
      return defaultVal;
    }
  }

  fetchBoolean(key: string): (boolean | undefined) {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as boolean;
    } else {
      return undefined;
    }
  }
  getBoolean(key: string, defaultVal: boolean): boolean {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as boolean;
    } else {
      return defaultVal;
    }
  }

  fetchString(key: string): (string | null) {
    // if(key in this._cache) {
    // if(this._cache.hasOwnProperty(key)) {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as string;
    } else {
      return null;
    }
  }
  getString(key: string, defaultVal: string): string {
    // if(key in this._cache) {
    // if(this._cache.hasOwnProperty(key)) {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as string;
    } else {
      return defaultVal;
    }
  }

  fetchBaseModel(key: string): (BaseModel | null) {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as BaseModel;
    } else {
      return null;
    }
  }
  getBaseModel(key: string, defaultVal: BaseModel): BaseModel {
    if(this._cache[key] !== undefined) {
      return this._cache[key] as BaseModel;
    } else {
      return defaultVal;
    }
  }

  // getObject(key: string, defaultVal: any = null): any {
  //   if(this._cache[key] !== undefined) {
  //     return this._cache[key];
  //   } else {
  //     return defaultVal;
  //   }
  // }
  fetchObject(key: string): any {
    if(this._cache[key] !== undefined) {
      return this._cache[key];
    } else {
      return null;
    }
  }
  getObject(key: string, defaultVal: any): any {
    if(this._cache[key] !== undefined) {
      return this._cache[key];
    } else {
      return defaultVal;
    }
  }

}
