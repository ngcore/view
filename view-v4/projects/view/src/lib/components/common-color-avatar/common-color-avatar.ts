import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { ColorTag, ColorTagUtil } from '@ngcore/core';

@Component({
  selector: 'common-color-avatar',
  template: `
<svg xmlns="http://www.w3.org/2000/svg" [style.height]="12*layerSize" width="72" height="72">
   <!-- Note the reverse order. Color sequence starts from the center and spans out... -->
   <circle [style.cx]="6*layerSize" [style.cy]="6*layerSize" cx="36" cy="36" [style.r]="6*layerSize" r="36" [style.fill]="color4" />
   <circle [style.cx]="6*layerSize" [style.cy]="6*layerSize" cx="36" cy="36" [style.r]="5*layerSize" r="30" [style.fill]="color3" />
   <circle [style.cx]="6*layerSize" [style.cy]="6*layerSize" cx="36" cy="36" [style.r]="4*layerSize" r="24" [style.fill]="color2" />
   <circle [style.cx]="6*layerSize" [style.cy]="6*layerSize" cx="36" cy="36" [style.r]="3*layerSize" r="18" [style.fill]="color1" />
   <circle [style.cx]="6*layerSize" [style.cy]="6*layerSize" cx="36" cy="36" [style.r]="2*layerSize" r="12" [style.fill]="color0" />
</svg> 
`
})
export class CommonColorAvatarComponent implements OnInit {

  // For convenience, we use the same nine color tag for Avatar as well.
  // But (for now) we only use the first 5 colors.
  colorTag: ColorTag;

  // contour radial width.
  // the center circle has radius = 2*layerSize, diameter = 4*layerSize.
  // all other rings have width = layerSize.
  // The diameter of the whole circle should be therefore 12*layerSize
  @Input("layer-size") layerSize: number;
  @Input("color-tag") itemCTag: string;

  // If set to true (typically, but not always, because the associated task is disabled or done, etc.)
  //   we display the "grayscale" image rather than the original color version.
  @Input("grayscale") isGrayscale: boolean;



  // To avoid error when colorTag is not properly set.
  get color0(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 0) ? this.colorTag.grayscaleColors[0].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 0) ? this.colorTag.colors[0].toString() : "#ffffff";
    }
  }
  get color1(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 1) ? this.colorTag.grayscaleColors[1].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 1) ? this.colorTag.colors[1].toString() : "#ffffff";
    }
  }
  get color2(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 2) ? this.colorTag.grayscaleColors[2].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 2) ? this.colorTag.colors[2].toString() : "#ffffff";
    }
  }
  get color3(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 3) ? this.colorTag.grayscaleColors[3].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 3) ? this.colorTag.colors[3].toString() : "#ffffff";
    }
  }
  get color4(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 4) ? this.colorTag.grayscaleColors[4].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 4) ? this.colorTag.colors[4].toString() : "#ffffff";
    }
  }
  // get color5(): string {
  //   if(this.isGrayscale) {
  //     return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 5) ? this.colorTag.grayscaleColors[5].toString() : "#ffffff";
  //   } else {
  //     return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 5) ? this.colorTag.colors[5].toString() : "#ffffff";
  //   }
  // }
  // get color6(): string {
  //   if(this.isGrayscale) {
  //     return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 6) ? this.colorTag.grayscaleColors[6].toString() : "#ffffff";
  //   } else {
  //     return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 6) ? this.colorTag.colors[6].toString() : "#ffffff";
  //   }
  // }
  // get color7(): string {
  //   if(this.isGrayscale) {
  //     return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 7) ? this.colorTag.grayscaleColors[7].toString() : "#ffffff";
  //   } else {
  //     return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 7) ? this.colorTag.colors[7].toString() : "#ffffff";
  //   }
  // }
  // get color8(): string {
  //   if(this.isGrayscale) {
  //     return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 8) ? this.colorTag.grayscaleColors[8].toString() : "#ffffff";
  //   } else {
  //     return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 8) ? this.colorTag.colors[8].toString() : "#ffffff";
  //   }
  // }


  constructor(
  ) {
    // if(isDL()) dl.log('Hello CommonColorAvatar Component');

    // testing...
    // this.colorTag = ColorTagUtil.randomColorTag();
    this.colorTag = ColorTag.BW_SILVER_SQUARE;
    this.layerSize = 5;
    // ..

    this.isGrayscale = false;
  }

  ngOnInit() {
    // variables can be read here....
    // if(isDL()) dl.log(">>>>>> this.layerSize = " + this.layerSize);
    // if(isDL()) dl.log(">>>>>> this.itemCTag = " + this.itemCTag);

    if(this.itemCTag) {
      let colorTag = ColorTag.fromString(this.itemCTag);
      this.colorTag = colorTag;
    }
  }

}
