import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { ColorTag, ColorTagUtil } from '@ngcore/core';


@Component({
  selector: 'mini-color-badge',
  template: `
<svg xmlns="http://www.w3.org/2000/svg" [style.width]="2*cellSize" [style.height]="2*cellSize" width="60" height="60">
  <rect [style.x]="0" [style.y]="0" x="0" y="0" [style.width]="cellSize" [style.height]="cellSize" width="20" height="30" [style.fill]="color0" />
  <rect [style.x]="cellSize" [style.y]="0" x="30" y="0" [style.width]="cellSize" [style.height]="cellSize" width="30" height="30" [style.fill]="color1" />
  <rect [style.x]="0" [style.y]="cellSize" x="0" y="30" [style.width]="cellSize" [style.height]="cellSize" width="30" height="30" [style.fill]="color3" />
  <rect [style.x]="cellSize" [style.y]="cellSize" x="30" y="30" [style.width]="cellSize" [style.height]="cellSize" width="30" height="30" [style.fill]="color4" />
</svg>
`
})
// <svg xmlns="http://www.w3.org/2000/svg" [style.width]="2*cellSize" [style.height]="2*cellSize" width="40" height="40">
//   <rect [style.x]="0" [style.y]="0" x="0" y="0" [style.width]="cellSize" [style.height]="cellSize" width="20" height="20" [style.fill]="color0" />
//   <rect [style.x]="cellSize" [style.y]="0" x="20" y="0" [style.width]="cellSize" [style.height]="cellSize" width="20" height="20" [style.fill]="color1" />
//   <rect [style.x]="0" [style.y]="cellSize" x="0" y="20" [style.width]="cellSize" [style.height]="cellSize" width="20" height="20" [style.fill]="color3" />
//   <rect [style.x]="cellSize" [style.y]="cellSize" x="20" y="20" [style.width]="cellSize" [style.height]="cellSize" width="20" height="20" [style.fill]="color4" />
// </svg>
export class MiniColorBadgeComponent implements OnInit {

  colorTag: ColorTag;

  @Input("cell-size") cellSize: number;
  // gridSize: number;
  @Input("color-tag") itemCTag: string;

  // If set to true (typically, but not always, because the associated task is disabled or done, etc.)
  //   we display the "grayscale" image rather than the original color version.
  @Input("grayscale") isGrayscale: boolean;
  

  // To avoid error when colorTag is not properly set.
  get color0(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 0) ? this.colorTag.grayscaleColors[0].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 0) ? this.colorTag.colors[0].toString() : "#ffffff";
    }
  }
  get color1(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 1) ? this.colorTag.grayscaleColors[1].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 1) ? this.colorTag.colors[1].toString() : "#ffffff";
    }
  }
  get color3(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 3) ? this.colorTag.grayscaleColors[3].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 3) ? this.colorTag.colors[3].toString() : "#ffffff";
    }
  }
  get color4(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 4) ? this.colorTag.grayscaleColors[4].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 4) ? this.colorTag.colors[4].toString() : "#ffffff";
    }
  }


  constructor(
  ) {
    // if(isDL()) dl.log('Hello MiniColorBadge Component');

    // testing...
    // this.colorTag = ColorTagUtil.randomColorTag();
    this.colorTag = ColorTag.BW_CHECKER_BOARD_A;
    // this.cellSize = 20;
    this.cellSize = 30;
    // this.gridSize = 2 * this.cellSize;

    this.isGrayscale = false;
  }

  ngOnInit() {
    // variables can be read here....
    // if(isDL()) dl.log(">>>>>> this.cellSize = " + this.cellSize);
    // if(isDL()) dl.log(">>>>>> this.gridSize = " + this.gridSize);

    if(this.itemCTag) {
      let colorTag = ColorTag.fromString(this.itemCTag);
      this.colorTag = colorTag;
    }
  }


}
