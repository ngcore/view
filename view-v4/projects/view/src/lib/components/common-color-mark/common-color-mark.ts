import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { ColorTag, ColorTagUtil } from '@ngcore/core';


@Component({
  selector: 'common-color-mark',
  template: `
<svg xmlns="http://www.w3.org/2000/svg" [style.width]="3*cellSize" [style.height]="3*cellSize" width="30" height="30">
  <rect [style.x]="0" [style.y]="0" x="0" y="0" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color0" />
  <rect [style.x]="cellSize" [style.y]="0" x="10" y="0" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color1" />
  <rect [style.x]="2*cellSize" [style.y]="0" x="20" y="0" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color2" />
  <rect [style.x]="0" [style.y]="cellSize" x="0" y="10" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color3" />
  <rect [style.x]="cellSize" [style.y]="cellSize" x="10" y="10" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color4" />
  <rect [style.x]="2*cellSize" [style.y]="cellSize" x="20" y="10" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color5" />
  <rect [style.x]="0" [style.y]="2*cellSize" x="0" y="20" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color6" />
  <rect [style.x]="cellSize" [style.y]="2*cellSize" x="10" y="20" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color7" />
  <rect [style.x]="2*cellSize" [style.y]="2*cellSize" x="20" y="20" [style.width]="cellSize" [style.height]="cellSize" width="10" height="10" [style.fill]="color8" />
</svg>
`
})
export class CommonColorMarkComponent implements OnInit {

  // TBD:
  // Make this 30x30 (cellSize==10) ????
  // ....

  colorTag: ColorTag;
  @Input("cell-size") cellSize: number;
  @Input("color-tag") itemCTag: string;

  // If set to true (typically, but not always, because the associated task is disabled or done, etc.)
  //   we display the "grayscale" image rather than the original color version.
  @Input("grayscale") isGrayscale: boolean;


  // To avoid error when colorTag is not properly set.
  get color0(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 0) ? this.colorTag.grayscaleColors[0].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 0) ? this.colorTag.colors[0].toString() : "#ffffff";
    }
  }
  get color1(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 1) ? this.colorTag.grayscaleColors[1].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 1) ? this.colorTag.colors[1].toString() : "#ffffff";
    }
  }
  get color2(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 2) ? this.colorTag.grayscaleColors[2].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 2) ? this.colorTag.colors[2].toString() : "#ffffff";
    }
  }
  get color3(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 3) ? this.colorTag.grayscaleColors[3].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 3) ? this.colorTag.colors[3].toString() : "#ffffff";
    }
  }
  get color4(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 4) ? this.colorTag.grayscaleColors[4].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 4) ? this.colorTag.colors[4].toString() : "#ffffff";
    }
  }
  get color5(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 5) ? this.colorTag.grayscaleColors[5].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 5) ? this.colorTag.colors[5].toString() : "#ffffff";
    }
  }
  get color6(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 6) ? this.colorTag.grayscaleColors[6].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 6) ? this.colorTag.colors[6].toString() : "#ffffff";
    }
  }
  get color7(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 7) ? this.colorTag.grayscaleColors[7].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 7) ? this.colorTag.colors[7].toString() : "#ffffff";
    }
  }
  get color8(): string {
    if(this.isGrayscale) {
      return (this.colorTag && this.colorTag.grayscaleColors && this.colorTag.grayscaleColors.length > 8) ? this.colorTag.grayscaleColors[8].toString() : "#ffffff";
    } else {
      return (this.colorTag && this.colorTag.colors && this.colorTag.colors.length > 8) ? this.colorTag.colors[8].toString() : "#ffffff";
    }
  }


  constructor(
  ) {
    // if(isDL()) dl.log('Hello CommonColorMark Component');

    // testing...
    // this.colorTag = ColorTagUtil.randomColorTag();
    this.colorTag = ColorTag.BW_SILVER_SQUARE;
    this.cellSize = 10;
    // this.gridSize = 3 * this.cellSize;

    this.isGrayscale = false;
  }

  ngOnInit() {
    // variables can be read here....
    // if(isDL()) dl.log(">>>>>> this.cellSize = " + this.cellSize);
    // if(isDL()) dl.log(">>>>>> this.itemCTag = " + this.itemCTag);

    if(this.itemCTag) {
      let colorTag = ColorTag.fromString(this.itemCTag);
      this.colorTag = colorTag;
    }
  }


}
