import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';
import { NgCoreAuthModule } from '@ngcore/auth';

import { CommonColorPointComponent } from './components/common-color-point/common-color-point';
import { CommonColorDotComponent } from './components/common-color-dot/common-color-dot';
import { CommonColorMarkComponent } from './components/common-color-mark/common-color-mark';
import { CommonColorTagComponent } from './components/common-color-tag/common-color-tag';
import { CommonColorBadgeComponent } from './components/common-color-badge/common-color-badge';
import { CommonColorAvatarComponent } from './components/common-color-avatar/common-color-avatar';
import { MiniColorPointComponent } from './components/mini-color-point/mini-color-point';
import { MiniColorDotComponent } from './components/mini-color-dot/mini-color-dot';
import { MiniColorMarkComponent } from './components/mini-color-mark/mini-color-mark';
import { MiniColorTagComponent } from './components/mini-color-tag/mini-color-tag';
import { MiniColorBadgeComponent } from './components/mini-color-badge/mini-color-badge';
import { SessionCacheService } from './services/session-cache-service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    // BrowserModule,

    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot(),
    NgCoreAuthModule.forRoot()
  ],
  declarations: [
    CommonColorPointComponent,
    CommonColorDotComponent,
    CommonColorMarkComponent,
    CommonColorTagComponent,
    CommonColorBadgeComponent,
    CommonColorAvatarComponent,
    MiniColorPointComponent,
    MiniColorDotComponent,
    MiniColorMarkComponent,
    MiniColorTagComponent,
    MiniColorBadgeComponent
  ],
  exports: [
    CommonColorPointComponent,
    CommonColorDotComponent,
    CommonColorMarkComponent,
    CommonColorTagComponent,
    CommonColorBadgeComponent,
    CommonColorAvatarComponent,
    MiniColorPointComponent,
    MiniColorDotComponent,
    MiniColorMarkComponent,
    MiniColorTagComponent,
    MiniColorBadgeComponent
  ]
})
export class NgCoreViewModule {
  static forRoot(): ModuleWithProviders<NgCoreViewModule> {
    return {
      ngModule: NgCoreViewModule,
      providers: [
        // SessionCacheService,
      ]
    };
  }
}
